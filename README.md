# efmtool

efmtool is a Java software for the enumeration of Elementary Flux Modes (EFMs) with MATLAB interface developed by Marco Terzer at ETH Zurich. A simple wrapper for python is available through `pip`.

## Instructions

### Java, MATLAB
See documentation at https://csb.ethz.ch/tools/software/efmtool.html.

### Python
Available on `pip`. See [code](./python) and [documentation](./python/README.md).