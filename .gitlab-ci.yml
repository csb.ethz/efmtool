# Based on the template at
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml

image: python:latest

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  GIT_DEPTH: 0
  GIT_STRATEGY: fetch
  GIT_FETCH_EXTRA_FLAGS: -f --tags --prune --update-head-ok

cache:
  paths:
    - .cache/pip
    - venv/

# Rules for triggering deployment. Based on https://stackoverflow.com/questions/62756669
workflow:
  rules:
    # Do no allow manually triggered pipelines to prevent duplicates!
    # Instead rerun the pipeline created with the last push
    - if: $CI_PIPELINE_SOURCE != "push"
      when: never
    # Only execute with a valid PEP-440 version tag. See
    # https://peps.python.org/pep-0440/#appendix-b-parsing-version-strings-with-regular-expressions
    - if: $CI_COMMIT_TAG =~ /^([1-9][0-9]*!)?(0|[1-9][0-9]*)(\.(0|[1-9][0-9]*))*((a|b|rc)(0|[1-9][0-9]*))?(\.post(0|[1-9][0-9]*))?(\.dev(0|[1-9][0-9]*))?$/
    - if: $CI_COMMIT_BRANCH

stages:
  - check_deployment
  - build
  - deploy

before_script:
  - export CI_LOG_LINE=$(git log --decorate=full| grep "^commit $CI_COMMIT_SHA[ ]")
  - export IS_ON_MAIN=$(echo $CI_LOG_LINE | grep -qso "origin/main, " && echo 1 || echo 0)
  - export COMMIT_ON_MAIN=$(git branch -r --contains $CI_COMMIT_SHA | grep -Eq '^[ ]+origin/main$'  && echo 1 || echo 0)
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate

check_deployment:
  stage: check_deployment
  script:
    # Make sure that tags are only applied to main branch.
    - if [[ ! -z "$CI_COMMIT_TAG" && $COMMIT_ON_MAIN != 1 ]]; then
      echo "Tags should never be applied to non main branches!" >&2;
      echo "We quit early! Please delete the tag, merge the branch to main and recreate the tag to continue" >&2;
      exit 1;
      fi

build:
  stage: build
  script:
    - git status
    - cd python
    - pip install build
    - python -m build
    - cd ..
  artifacts:
    paths:
      - python/dist/
  dependencies:
    - check_deployment

deploy:
  stage: deploy
  script:
    - pip install twine
    - twine upload --repository pypi python/dist/*
  environment: production
  rules:
    - if: $CI_COMMIT_TAG
  dependencies:
    - build
